function onDOMContentLoaded() {
    let mavenExample = document.getElementById("maven-example");
    let mavenExampleCover = document.createElement("div");
    mavenExampleCover.className = "pending-maven-upload";
    mavenExampleCover.style.top = "-" + (mavenExample.clientHeight) + "px";
    mavenExampleCover.innerText = "Pending Maven Upload";
    mavenExample.insertAdjacentElement("afterend", mavenExampleCover);
}
if (document.readyState === "loading") {
    console.info("DOM still loading");
    document.addEventListener("DOMContentLoaded", onDOMContentLoaded);
} else {
    console.info("DOM already loaded");
    onDOMContentLoaded();
}